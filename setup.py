import setuptools

setuptools.setup(
    setup_requires=['setuptools-odoo'],
    odoo_addon={
        'depends_override': {
            'partner_firstname': 'odoo10-addon-partner-firstname',
        },
    },
    dependency_links=[
        'https://wheelhouse.odoo-community.org/oca/'  # Not working for some reason..
    ]
)
