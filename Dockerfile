FROM odoo:latest

USER root
RUN apt update && apt install -y --no-install-recommends git

WORKDIR /tmp/
COPY ./ /tmp/
RUN pip install -r requirements.txt .

USER odoo
